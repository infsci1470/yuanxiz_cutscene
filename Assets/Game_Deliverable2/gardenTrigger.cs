using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gardenTrigger : MonoBehaviour
{
    public bool playerInRange;

    void Start()
    {
        playerInRange = false;
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            playerInRange = true;
        }
    }
    void OnTriggerExit(Collider collider)
    {
        playerInRange = false;
   
    }
}