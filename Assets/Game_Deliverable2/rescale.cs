using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rescale : MonoBehaviour
{
    Vector3 minScale;
    public Vector3 maxScale;
    public float  speed = 2f;
    public float duration = 5f;

    public gardenTrigger gardenTrigger;

    void Update()
    {
        StartCoroutine(generateEvent());
    }
    

    IEnumerator generateEvent()
    {
        minScale = transform. localScale;
        if(gardenTrigger.playerInRange)
            yield return startLerp(minScale, maxScale, duration);
    }


    public IEnumerator startLerp(Vector3 a, Vector3 b, float time){
        float i = 0.0f;
        float rate = (1.0f / time) * speed;
        while(i<1.0f){
            i+=Time.deltaTime * rate;
            transform.localScale = Vector3.Lerp(a, b, i);
            yield return null;
        }
    }
}
