using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class foxTrigger : MonoBehaviour
{
    public Animator fox;
    public bool playerInRange;

    void Start()
    {
        playerInRange = false;

    }
    void Update()
    {
     
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Player")        
        {
            playerInRange = true;
        }
    }
    void OnTriggerExit(Collider collider)
    {
        playerInRange = false;
    }
}
