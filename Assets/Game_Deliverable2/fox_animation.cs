using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fox_animation : MonoBehaviour
{
    // Start is called before the first frame update
    private Animator animator;
    public foxTrigger fox;
    public path1_trigger trigger1;
    public path2_trigger trigger2;

    private string path1 = "Fox_Run";
    private string path2  = "Fox_Run 0";
    private bool flag;
    private bool pathComplete;
    private bool flag2;
    private bool flag3;

    void Start()
    {
        animator = GetComponent<Animator>();
        flag = true;
        flag2 = false;

        animator.SetInteger("count", 0);

    }

    // Update is called once per frame
    void Update()
    {
        if (fox.playerInRange )
        {
            if (flag)
            {
                animator.Play(path1);
                animator.SetInteger("count", 1);
                flag = false;
            }
            else if (animator.GetInteger("count") == 2 && !flag && !pathComplete)
            {
                animator.Play(path2);
                pathComplete = true;
            }
            else if (pathComplete && flag2)
            {
                flag3 = true;
            }
           
        }
        if (trigger1.inRange)
        {
            animator.SetInteger("count", 2);
        }

        if(trigger2.inRange)
        {
            flag2 = true;
        }

    }
}
