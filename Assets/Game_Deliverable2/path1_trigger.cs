using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class path1_trigger : MonoBehaviour
{
    public bool inRange;
    public GameObject panel;

    void Start()
    {
        inRange = false;
    }

   
    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Respawn")
        {
            inRange = true;
        }
    }
    void OnTriggerExit(Collider collider)
    {
        inRange = false;
    }
}
