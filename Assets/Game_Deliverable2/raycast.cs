using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class raycast : MonoBehaviour
{
    public float range = 5;
    
    public GameObject panel;

    void Start(){
        panel.SetActive(false);
    }

  
    void Update()
    {
        Vector3 direction = Vector3.forward;
        Ray theRay = new Ray(transform.position, transform.TransformDirection(direction*range));
        Debug.DrawRay(transform.position, transform.TransformDirection(direction*range));

        if(Physics.Raycast(theRay, out RaycastHit hit, range))
        {
            if(hit.collider.tag == "danger")
            {
                panel.SetActive(true);
            }else{
                panel.SetActive(false);
            }
        }
    }
}
