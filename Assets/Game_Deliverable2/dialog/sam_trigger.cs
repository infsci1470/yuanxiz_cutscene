using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class sam_trigger : MonoBehaviour
{
    public GameObject panel;
    public GameObject buttonE;
    public bool playerInRange;

    private int trackerSam;

    void Start()
    {
        playerInRange = false;
        panel.SetActive(false);
        trackerSam = 0;
    }

    void Update()
    {
        if (buttonE.activeSelf && Input.GetKeyDown(KeyCode.E) && playerInRange)
        {
            panel.SetActive(true);
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            buttonE.SetActive(true);
            playerInRange = true;
            trackerSam = 0;
        }
    }
    void OnTriggerExit(Collider collider)
    {
        buttonE.SetActive(false);
        panel.SetActive(false);
        playerInRange = false;
        trackerSam = 0;
    }

    public bool isTriggerSam()
    {
        if (playerInRange && trackerSam == 0)
        {
            trackerSam += 1;
            return true;
        }
        return false;
    }

}