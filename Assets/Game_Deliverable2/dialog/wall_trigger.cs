using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class wall_trigger : MonoBehaviour
{
    public GameObject panel;
    private bool playerInRange;

    void Start()
    {
        playerInRange = false;
        panel.SetActive(false);

    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            panel.SetActive(true);
            playerInRange = true;
        }
    }
    void OnTriggerExit(Collider collider)
    {
        panel.SetActive(false);
        playerInRange = false;
   
    }
}