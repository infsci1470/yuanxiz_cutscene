using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class samText : MonoBehaviour
{
    public NPC npc;

    public GameObject panel;
    public Text npcName;
    public Text content;

    public sam_trigger trigger;

    public Queue<string> dialogListSam;

    void Update()
    {
        if (trigger.isTriggerSam())
        {
            StartConversation();
        }
        if (Input.GetMouseButtonDown(0))
        {
            content.text = dialogListSam.Dequeue();
        }

        if (dialogListSam.Count == 0)
        {
            panel.SetActive(false);
        }

    }
    void StartConversation()
    {
        npcName.text = npc.name;
        dialogListSam = getQueue(npc.dialogue);
        content.text = dialogListSam.Dequeue();
    }
    public Queue<string> getQueue(string[] list)
    {
        Queue<string> queue = new Queue<string>(list);
        return queue;
    }


}
