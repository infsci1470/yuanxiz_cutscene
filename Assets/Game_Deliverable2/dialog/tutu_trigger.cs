using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class tutu_trigger : MonoBehaviour
{
    public GameObject panel;
    public GameObject buttonE;
    public bool playerInRange;

    private int tracker;

    void Start()
    {
        playerInRange = false;
        panel.SetActive(false);
        buttonE.SetActive(false);
        tracker = 0;
    }

    void Update()
    {
        if(buttonE.activeSelf && Input.GetKeyDown(KeyCode.E) && playerInRange)
        {
            panel.SetActive(true);
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            buttonE.SetActive(true);
            playerInRange = true;
            tracker = 0;
        }
    }
    void OnTriggerExit(Collider collider)
    {
        buttonE.SetActive(false);
        panel.SetActive(false);
        playerInRange = false;
        tracker = 0;
    }

    public bool isTriggerTutu()
    {
        if (playerInRange && tracker == 0)
        {
            tracker += 1;
            return true;
        }
        return false;
    }

}
