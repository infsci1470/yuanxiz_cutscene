using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_manager : MonoBehaviour
{
    public NPC tutu;
    public NPC sam;
    public NPC alice;

    public GameObject panel;
    public Text npcName;
    public Text content;

    public sam_trigger triggerSam;
    public tutu_trigger triggerTutu;
    public alice_trigger triggerAlice;

    public path2_trigger path2_trigger;


    public Queue<string> dialogList;

    public GameObject MenuPanel;
    private bool menuActive;

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Tab))
        {
            if(menuActive==false)
            {
                MenuPanel.SetActive(true);
                menuActive = true;
            }else{
                MenuPanel.SetActive(false);
                menuActive = false;
            }
        }
     
        if (triggerSam.isTriggerSam())
        {
            StartConversationSam();
        }
        if (triggerTutu.isTriggerTutu())
        {
            StartConversationTutu();
        }

        if(triggerAlice.isTriggerAlice() && path2_trigger.inRange)
        {
            StartConversationAlice();
        }

        if (Input.GetMouseButtonDown(0))
        {
            content.text = dialogList.Dequeue();
        }

        if (dialogList.Count == 0)
        {
            dialogList.Clear();
            panel.SetActive(false);
        }

    }
    
    void StartConversationSam()
    {
        npcName.text = sam.name;
        dialogList = getQueue(sam.dialogue);
        content.text = dialogList.Dequeue();
    }

    void StartConversationTutu()
    {
        npcName.text = tutu.name;
        dialogList = getQueue(tutu.dialogue);
        content.text = dialogList.Dequeue();
    }

    void StartConversationAlice()
    {
        npcName.text = alice.name;
        dialogList = getQueue(alice.dialogue);
        content.text = dialogList.Dequeue();
    }
    public Queue<string> getQueue(string[] list)
    {
        Queue<string> queue = new Queue<string>(list);
        return queue;
    }


}
