using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class tutuText : MonoBehaviour
{
    public NPC npc;

    public GameObject panel;
    public Text npcName;
    public Text content;

    public tutu_trigger trigger;

    public Queue<string> dialogListTutu;

    void Update()
    {
        if (trigger.isTriggerTutu())
        {
            StartConversation();
        }
        if (Input.GetMouseButtonDown(0))
        {
            content.text = dialogListTutu.Dequeue();
        }

        if (dialogListTutu.Count == 0)
        {
            panel.SetActive(false);
        }

    }
    void StartConversation()
    {
        npcName.text = npc.name;
      
        dialogListTutu = getQueue(npc.dialogue);
        content.text = dialogListTutu.Dequeue();
    }
    public Queue<string> getQueue(string[] list)
    {
        Queue<string> queue = new Queue<string>(list);
        return queue;
    }


}
