using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class alice_trigger : MonoBehaviour
{
    public GameObject panel;
    public GameObject dialogPanel;
    public GameObject buttonE;
    public bool playerInRange;

    public path2_trigger path2_trigger;

    private int tracker;

    void Start()
    {
        playerInRange = false;
        panel.SetActive(false);
        dialogPanel.SetActive(false);
        tracker = 0;
    }

    void Update()
    {
        if (buttonE.activeSelf && Input.GetKeyDown(KeyCode.E) && playerInRange)
        {
            if (path2_trigger.inRange)
            {
                dialogPanel.SetActive(true);
            }
            else if(!path2_trigger.inRange)
            {
                panel.SetActive(true);
            }
        }

    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            buttonE.SetActive(true);
            playerInRange = true;
            tracker = 0;
        }
    }
    void OnTriggerExit(Collider collider)
    {
        buttonE.SetActive(false);
        panel.SetActive(false);
        dialogPanel.SetActive(false);
        playerInRange = false;
        tracker = 0;
    }

    public bool isTriggerAlice()
    {
        if (playerInRange && tracker == 0)
        {
            tracker += 1;
            return true;
        }
        return false;
    }

}