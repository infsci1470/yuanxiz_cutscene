using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Control_move : MonoBehaviour
{
    public CharacterController controller;
    //public Transform cam;

    public float speed ;
    public float turnSmoothTime;

    float turnSmoothVelocity;

    public Transform cam;

    
    void Start()
    {

    }
   
    void Update()
    {
       float horizontal = Input.GetAxisRaw("Horizontal"); 
       float vertical = Input.GetAxisRaw("Vertical"); 

       Vector3 movementDirection = new Vector3(horizontal, 0f, vertical).normalized;


       if (movementDirection.magnitude >= 0.1f )
       {
            float targetAngle = Mathf.Atan2(movementDirection.x, movementDirection.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);

            transform.rotation = Quaternion.Euler(0f, angle, 0f);

            Vector3 moveDir = Quaternion.Euler(0, targetAngle, 0) * Vector3.forward;
            controller.Move(moveDir * speed * Time.deltaTime);

       }

    }

    
}
